package Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import startEnd.beforeAfter;

public class utilities extends beforeAfter {

	public static String readAttribute(String input) throws IOException {
		String property1 = null;
		InputStream inputstream;
		Properties prop = new Properties();

		inputstream = new FileInputStream("./Config/config.properties");
		if (inputstream != null) {
			prop.load(inputstream);
			property1 = prop.getProperty(input);

		}
		return property1;
	}

	public static WebElement driverCallingID(String InputID) throws IOException {
		WebElement Input_id = driver.findElement(By.id(utilities.readAttribute(InputID)));
		return Input_id;
	}

	public static WebElement driverCallingXpath(String InputXpath) throws IOException {

		WebElement Input_xpath = driver.findElement(By.xpath(utilities.readAttribute(InputXpath)));
		return Input_xpath;
	}

	public static WebElement driverCallingLinkText(String InputLinkText) throws IOException {

		WebElement Input_link = driver.findElement(By.linkText(utilities.readAttribute(InputLinkText)));
		return Input_link;
	}

	public static String readExcel(int column) throws IOException {

		// Create an object of File class to open xlsx file

		// String fileName = "Input.xlsx";
		// File file = new File("./TestData/" + fileName);

		FileInputStream inputStream = new FileInputStream("./TestData/Input.xlsx");

		XSSFWorkbook guru99Workbook = new XSSFWorkbook(inputStream);

		// String fileExtensionName = fileName.substring(fileName.indexOf("."));

		XSSFSheet sheet = guru99Workbook.getSheet("Sheet1");

		System.out.println(sheet.getPhysicalNumberOfRows());

		XSSFRow row = sheet.getRow(0);

		//Cell cell = row.getCell(0);
		XSSFCell cell = null;
		// DataFormatter formatter = new DataFormatter();

		// String empno = formatter.formatCellValue(value);

		//String actualValue = value.getStringCellValue();
		int colNum = -1;
		int w=row.getLastCellNum();
		for(int i=0; i<=row.getLastCellNum(); i++)
		{
			if(row.getCell(i).getStringCellValue().trim().equals("EmpID"))
			{
				colNum= i;	
			break;
			}
		}
		
		
		row=sheet.getRow(1);
		cell=row.getCell(colNum);
		String  actualValue = cell.getStringCellValue();
		System.out.println(actualValue);
		return actualValue;
		

	}
}
