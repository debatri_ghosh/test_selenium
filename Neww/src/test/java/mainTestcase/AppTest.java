package mainTestcase;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Implementations.implementation;
import startEnd.beforeAfter;

/**
 * Unit test for simple App.
 */
public class AppTest extends beforeAfter {
	//implementation fn1 = new implementation();
	
	@Test
	void test() throws IOException {
		
		implementation.fn();
		implementation.fn2();
		
	}

	@Test
	void test1() {
		implementation.fn3();
	}
	
	@Test
	void test2() throws IOException {
		implementation.fn4();
	}

}
